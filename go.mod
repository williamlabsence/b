module modernc.org/b

go 1.18

require (
	modernc.org/fileutil v1.2.0
	modernc.org/mathutil v1.6.0
	modernc.org/strutil v1.2.0
)

require github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
